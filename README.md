phpCAS BSSN
=======
Install menggunakan composer
`composer require bssn-sso/phpcas`

Buat direktori SSO di App. Sehingga direktori akan terlihat seperti `App\SSO`.
Copy file konfigurasi SSO  `SSO.php` yang ada di dalam Example ke direktori `App\SSO`

Cara penggunaan di laravel:
```
use App\Sso;
```

```
Sso::authenticate(); // code ini akan mengarahkan web browser ke halaman login BSSN
$user = Sso::getUser(); //code ini akan mengambil data user yang ada di dalam atribut SSO
```